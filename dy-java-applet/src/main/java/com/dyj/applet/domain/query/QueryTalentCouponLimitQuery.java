package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.UserInfoQuery;

/**
 * 查询主播发券配置信息请求值
 */
public class QueryTalentCouponLimitQuery extends UserInfoQuery {


    /**
     * <p>账号类型：取值1时：talent_account 字段必填 取值2是：open_id 字段必填
     */
    private Integer account_type;
    /**
     * <p>小程序appid</p>
     */
    private String app_id;
    /**
     * <p>抖音开平券模板id</p>
     */
    private String coupon_meta_id;

    /**
     * <p>主播抖音号，account_type为1时必填</p> 选填
     */
    private String talent_account;


    public Integer getAccount_type() {
        return account_type;
    }

    public QueryTalentCouponLimitQuery setAccount_type(Integer account_type) {
        this.account_type = account_type;
        return this;
    }

    public String getApp_id() {
        return app_id;
    }

    public QueryTalentCouponLimitQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public QueryTalentCouponLimitQuery setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public String getTalent_account() {
        return talent_account;
    }

    public QueryTalentCouponLimitQuery setTalent_account(String talent_account) {
        this.talent_account = talent_account;
        return this;
    }

    public static QueryTalentCouponLimitQueryBuilder builder(){
        return new QueryTalentCouponLimitQueryBuilder();
    }


    public static final class QueryTalentCouponLimitQueryBuilder {
        private Integer account_type;
        private String app_id;
        private String coupon_meta_id;
        private String talent_account;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private QueryTalentCouponLimitQueryBuilder() {
        }

        public static QueryTalentCouponLimitQueryBuilder aQueryTalentCouponLimitQuery() {
            return new QueryTalentCouponLimitQueryBuilder();
        }

        public QueryTalentCouponLimitQueryBuilder accountType(Integer accountType) {
            this.account_type = accountType;
            return this;
        }

        public QueryTalentCouponLimitQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public QueryTalentCouponLimitQueryBuilder couponMetaId(String couponMetaId) {
            this.coupon_meta_id = couponMetaId;
            return this;
        }

        public QueryTalentCouponLimitQueryBuilder talentAccount(String talentAccount) {
            this.talent_account = talentAccount;
            return this;
        }

        public QueryTalentCouponLimitQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public QueryTalentCouponLimitQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryTalentCouponLimitQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryTalentCouponLimitQuery build() {
            QueryTalentCouponLimitQuery queryTalentCouponLimitQuery = new QueryTalentCouponLimitQuery();
            queryTalentCouponLimitQuery.setAccount_type(account_type);
            queryTalentCouponLimitQuery.setApp_id(app_id);
            queryTalentCouponLimitQuery.setCoupon_meta_id(coupon_meta_id);
            queryTalentCouponLimitQuery.setTalent_account(talent_account);
            queryTalentCouponLimitQuery.setOpen_id(open_id);
            queryTalentCouponLimitQuery.setTenantId(tenantId);
            queryTalentCouponLimitQuery.setClientKey(clientKey);
            return queryTalentCouponLimitQuery;
        }
    }
}
