package com.dyj.applet.domain;

import java.util.List;

/**
 * 商家资质信息
 *
 * @author danmo
 * @date 2024-04-28 13:34
 **/
public class SupplierCustomerInfo {


    /**
     * 服务商和商家合作协议/授意书
     */
    private PowerOfAttorney power_of_attorney;

    /**
     * 商家营业执照
     */
    private BusinessLicense business_license;

    /**
     * 行业许可证
     */
    private List<IndustryLicense> industry_license;

    /**
     * 其他补充材料
     */
    private List<SupplierCustomerOtherInfo> other_info;

    public static SupplierCustomerInfoBuilder builder() {
        return new SupplierCustomerInfoBuilder();
    }

    public static class SupplierCustomerInfoBuilder {
        private PowerOfAttorney powerOfAttorney;
        private BusinessLicense businessLicense;
        private List<IndustryLicense> industryLicense;
        private List<SupplierCustomerOtherInfo> otherInfo;

        public SupplierCustomerInfoBuilder powerOfAttor(PowerOfAttorney powerOfAttorney) {
            this.powerOfAttorney = powerOfAttorney;
            return this;
        }

        public SupplierCustomerInfoBuilder businessLicens(BusinessLicense businessLicense) {
            this.businessLicense = businessLicense;
            return this;
        }

        public SupplierCustomerInfoBuilder industryLicense(List<IndustryLicense> industryLicense) {
            this.industryLicense = industryLicense;
            return this;
        }

        public SupplierCustomerInfoBuilder otherInfo(List<SupplierCustomerOtherInfo> otherInfo) {
            this.otherInfo = otherInfo;
            return this;
        }

        public SupplierCustomerInfo build() {
            SupplierCustomerInfo supplierCustomerInfo = new SupplierCustomerInfo();
            supplierCustomerInfo.setPower_of_attorney(powerOfAttorney);
            supplierCustomerInfo.setBusiness_license(businessLicense);
            supplierCustomerInfo.setIndustry_license(industryLicense);
            supplierCustomerInfo.setOther_info(otherInfo);
            return supplierCustomerInfo;
        }
    }


    public PowerOfAttorney getPower_of_attorney() {
        return power_of_attorney;
    }

    public void setPower_of_attorney(PowerOfAttorney power_of_attorney) {
        this.power_of_attorney = power_of_attorney;
    }

    public BusinessLicense getBusiness_license() {
        return business_license;
    }

    public void setBusiness_license(BusinessLicense business_license) {
        this.business_license = business_license;
    }

    public List<IndustryLicense> getIndustry_license() {
        return industry_license;
    }

    public void setIndustry_license(List<IndustryLicense> industry_license) {
        this.industry_license = industry_license;
    }

    public List<SupplierCustomerOtherInfo> getOther_info() {
        return other_info;
    }

    public void setOther_info(List<SupplierCustomerOtherInfo> other_info) {
        this.other_info = other_info;
    }
}
