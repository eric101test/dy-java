package com.dyj.applet.domain;

/**
 * 优待政策
 * @author danmo
 * @date 2024-04-28 14:25
 **/
public class ScenicNoticePreferentialPolicy {

    /**
     * 优待政策-适用条件(不超过200个汉字)
     */
    private String condition;
    /**
     * 优待政策-优待内容(免费、优惠、半价)
     */
    private String discount;
    /**
     * 优待政策-人群(婴儿、儿童、老人、军人、残障人士，不超过30个汉字)
     */
    private String population;

    public static ScenicNoticePreferentialPolicyBuilder builder() {
        return new ScenicNoticePreferentialPolicyBuilder();
    }
    public static class ScenicNoticePreferentialPolicyBuilder {
        private String condition;
        private String discount;
        private String population;
        public ScenicNoticePreferentialPolicyBuilder condition(String condition) {
            this.condition = condition;
            return this;
        }
        public ScenicNoticePreferentialPolicyBuilder discount(String discount) {
            this.discount = discount;
            return this;
        }
        public ScenicNoticePreferentialPolicyBuilder population(String population) {
            this.population = population;
            return this;
        }
        public ScenicNoticePreferentialPolicy build() {
            ScenicNoticePreferentialPolicy scenicNoticePreferentialPolicy = new ScenicNoticePreferentialPolicy();
            scenicNoticePreferentialPolicy.setCondition(condition);
            scenicNoticePreferentialPolicy.setDiscount(discount);
            scenicNoticePreferentialPolicy.setPopulation(population);
            return scenicNoticePreferentialPolicy;
        }
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }
}
