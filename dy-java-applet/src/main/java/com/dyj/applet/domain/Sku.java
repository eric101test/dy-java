package com.dyj.applet.domain;

import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

import java.util.Objects;

/**
 * @author danmo
 * @date 2024-04-29 14:31
 **/
public class Sku {

    /**
     * SKU属性字段
     */
    private JSONObject attributes;

    /**
     * 价格(人民币分)
     */
    private Long price;

    /**
     * 在线状态 1 - 在线; 2 - 下线
     */
    private Integer status;

    /**
     * 库存数量
     */
    private Long stock;

    public static SkuBuilder builder() {
        return new SkuBuilder();
    }

    public static class SkuBuilder {
        private JSONObject attributes;
        private Long price;
        private Integer status;
        private Long stock;

        public SkuBuilder attributes(String attribute) {
            if(Objects.isNull(attribute)){
                this.attributes = new JSONObject();
            }
            if(StringUtils.hasLength(attribute)){
                this.attributes.put("1301", attribute);
            }
            return this;
        }

        public SkuBuilder price(Long price) {
            this.price = price;
            return this;
        }

        public SkuBuilder status(Integer status) {
            this.status = status;
            return this;
        }

        public SkuBuilder stock(Long stock) {
            this.stock = stock;
            return this;
        }

        public Sku build() {
            Sku sku = new Sku();
            sku.setAttributes(attributes);
            sku.setPrice(price);
            sku.setStatus(status);
            sku.setStock(stock);
            return sku;
        }
    }

    public JSONObject getAttributes() {
        return attributes;
    }

    public void setAttributes(JSONObject attributes) {
        this.attributes = attributes;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }
}
